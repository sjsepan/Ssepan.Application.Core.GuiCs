﻿using System;
using System.IO;
using Ssepan.Io.Core;
using Ssepan.Utility.Core;
//using Microsoft.Data.ConnectionUI;//cannot use until Microsoft.VisualStudio.Data.dll is made redistributable by Microsoft
using System.Reflection;
using NStack;
using Terminal.Gui;

namespace Ssepan.Application.Core.GuiCs
{
    public static class Dialogs
    {
        #region Declarations
        public const string FilterSeparator = "|";
        public const string FilterDescription = "{0} Files(s)";
        public const string FilterFormat = "{0} (*.{1})|*.{1}";
        #endregion Declarations

        #region Methods
        /// <summary>
        /// Get path to save data.
        /// Static.
        /// </summary>
        /// <param name="fileDialogInfo">ref FileDialogInfo<Window, string></param>
        /// <param name="errorMessage">ref string</param>
        public static bool GetPathForSave
        (
            ref FileDialogInfo<Window, string> fileDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			string fileResponse = null;
            string messageTemp = null;
            // string[] nameAndPattern = null;

            try
            {
				SaveDialog saveDialog;
				using
				(
					saveDialog = new SaveDialog()
				)
				{
					if
					(
						fileDialogInfo.Filename.EndsWith(fileDialogInfo.NewFilename)
						||
						fileDialogInfo.ForceDialog
					)
					{
						saveDialog.Title = fileDialogInfo.Title;//(fileDialogInfo.ForceDialog ? "Save As..." : "Save...")
						saveDialog.Message = "Save a file";

						//saveDialog.AllowedFileTypes = new string[] {};
						saveDialog.AllowsOtherFileTypes = true;
						saveDialog.DirectoryPath =

							fileDialogInfo.InitialDirectory == default
							?
							fileDialogInfo.CustomInitialDirectory
							:
							Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
						;
						saveDialog.FilePath = fileDialogInfo.Filename;

						//TODO:filters
						// foreach (string filter in fileDialogInfo.Filters.Split(FileDialogInfo<Window, string>.FILTER_SEPARATOR))
						// {
						//     nameAndPattern = filter.Split(FileDialogInfo<Window, string>.FILTER_ITEM_SEPARATOR);
						//     FileFilter fileFilter = new FileFilter();
						//     Debug.Assert(fileFilter != null);
						//     fileFilter.Name = nameAndPattern[0];
						//     fileFilter.Extensions = new string[] { nameAndPattern[1] };
						//     saveDialog.Filters.Add(fileFilter);
						// }

						Terminal.Gui.Application.Run(saveDialog);

						if (!saveDialog.Canceled)
						{
							fileResponse = saveDialog.FilePath.ToString();

							if (!string.IsNullOrWhiteSpace(fileResponse))
							{
								if (string.Equals(Path.GetFileName(fileResponse), fileDialogInfo.NewFilename, StringComparison.CurrentCultureIgnoreCase))
								{
									//user did not select or enter a name different than new; for now I have chosen not to allow that name to be used for a file.--SJS, 12/16/2005
									messageTemp = string.Format("The name \"{0}.{1}\" is not allowed; please choose another. Settings not saved.", fileDialogInfo.NewFilename.ToLower(), fileDialogInfo.Extension.ToLower());
									MessageDialogInfo<Window, string, object, string, ustring[]> messageDialogInfo =
										new										(
											parent: fileDialogInfo.Parent,
											modal: fileDialogInfo.Modal,
											title: fileDialogInfo.Title,
											dialogFlags: null,
											messageType: "Info",
											buttonsType: null,
											message: messageTemp,
											response: default //ignored by this caller
										);
									ShowMessageDialog
									(
										ref messageDialogInfo,
										ref errorMessage
									);

									//Forced cancel
									fileDialogInfo.Response = "Cancel";
								}
								else
								{
									//set new filename
									fileDialogInfo.Filename = fileResponse;
									fileDialogInfo.Filenames = [fileResponse];
									fileDialogInfo.Response = "Ok";
									returnValue = true;
								}
							}
							else
							{
								//treat as Cancel
								fileDialogInfo.Response = "Cancel";
								returnValue = true;
							}
						}
						else
						{
							fileDialogInfo.Response = "Cancel";
							returnValue = true;
						}
					}
					else
					{
						fileDialogInfo.Response = "Ok";
						returnValue = true;
					}
				}
			}
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            fileDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get path to load data.
        /// Static.
        /// </summary>
        /// <param name="fileDialogInfo">ref FileDialogInfo<Window, string></param>
        /// <param name="errorMessage">ref string</param>
        public static bool GetPathForLoad
        (
            ref FileDialogInfo<Window, string> fileDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			string fileResponse = null;
			// string[] nameAndPattern = null;

			try
            {
                if (fileDialogInfo.ForceNew)
                {
                    fileDialogInfo.Filename = fileDialogInfo.NewFilename;

                    returnValue = true;
                }
                else
                {
					OpenDialog openDialog;
					using
					(

						openDialog = new OpenDialog()
					)
					{
						openDialog.Title = fileDialogInfo.Title;
						openDialog.Message = "Open a file";

						//AllowedFileTypes = new string[] {},
						openDialog.AllowsOtherFileTypes = true;
						openDialog.AllowsMultipleSelection = true;
						openDialog.CanChooseDirectories = fileDialogInfo.SelectFolders;//false;
						openDialog.CanChooseFiles = !fileDialogInfo.SelectFolders;//true;
						openDialog.DirectoryPath =

							fileDialogInfo.InitialDirectory == default
							?
							fileDialogInfo.CustomInitialDirectory
							:
							Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
						;
						openDialog.FilePath = fileDialogInfo.Filename;

						// foreach (string filter in fileDialogInfo.Filters.Split(FileDialogInfo<Window, DialogResult>.FILTER_SEPARATOR))
						// {
						//     nameAndPattern = filter.Split(FileDialogInfo<Window, DialogResult>.FILTER_ITEM_SEPARATOR);
						//     FileFilter fileFilter = new FileFilter ();
						//     fileFilter.Name = nameAndPattern[0];
						//     fileFilter.Extensions = new string[] { nameAndPattern[1] };
						//     openFileDialog.Filters.Add(fileFilter);                            
						// }

						Terminal.Gui.Application.Run(openDialog);

						if (!openDialog.Canceled)
						{
							fileResponse = openDialog.FilePath.ToString();

							if (!string.IsNullOrWhiteSpace(fileResponse))
							{
								//set new filename
								fileDialogInfo.Filename = fileResponse;
								fileDialogInfo.Filenames = [fileResponse];
								fileDialogInfo.Response = "Ok";
								returnValue = true;
							}
							else
							{
								//treat as Cancel
								fileDialogInfo.Response = "Cancel";
								returnValue = true;
							}
						}
						else
						{
							fileDialogInfo.Response = "Cancel";
							returnValue = true;
						}
					}
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            fileDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Select a folder path.
        /// Static.
        /// </summary>
        /// <param name="fileDialogInfo">ref FileDialogInfo<Window, string>. returns path in Filename property</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetFolderPath
        (
            ref FileDialogInfo<Window, string> fileDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			string fileResponse = null;

			try
            {
				OpenDialog openDialog;
				using
				(

					openDialog = new OpenDialog()
				)
				{
					openDialog.Title = fileDialogInfo.Title;
					openDialog.Message = "Open a file";

					//AllowedFileTypes = new string[] {},
					openDialog.AllowsOtherFileTypes = true;
					openDialog.AllowsMultipleSelection = true;
					openDialog.CanChooseDirectories = fileDialogInfo.SelectFolders;//false;
					openDialog.CanChooseFiles = !fileDialogInfo.SelectFolders;//true;
					openDialog.DirectoryPath =

						fileDialogInfo.InitialDirectory == default
						?
						fileDialogInfo.CustomInitialDirectory
						:
						Environment.GetFolderPath(Environment.SpecialFolder.Personal).WithTrailingSeparator()
					;
					openDialog.FilePath = string.Empty;//fileDialogInfo.Filename;

					Terminal.Gui.Application.Run(openDialog);

					if (!openDialog.Canceled)
					{
						fileResponse = openDialog.FilePath.ToString();

						if (!string.IsNullOrWhiteSpace(fileResponse))
						{
							//set new filename
							fileDialogInfo.Filename = fileResponse;
							fileDialogInfo.Filenames = [fileResponse];
							fileDialogInfo.Response = "Ok";
							returnValue = true;
						}
						else
						{
							//treat as Cancel
							fileDialogInfo.Response = "Cancel";
							returnValue = true;
						}
					}
					else
					{
						fileDialogInfo.Response = "Cancel";
						returnValue = true;
					}
				}
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            fileDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Shows an About dialog.
        /// Static.
        /// </summary>
        /// <param name="aboutDialogInfo">ref AboutDialogInfo<Window, string, object></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool ShowAboutDialog
        (
            ref AboutDialogInfo<Window, string, object> aboutDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			string response = null;

            try
            {
				string messageTemp =
                    aboutDialogInfo.Title + ": \n" +
                    "ProgramName: " + aboutDialogInfo.ProgramName + "\n" +
                    "Version: : " + aboutDialogInfo.Version + "\n" +
                    "Copyright: " + aboutDialogInfo.Copyright + "\n" +
                    "Comments: : " + aboutDialogInfo.Comments + "\n" +
                    "Website: : " + aboutDialogInfo.Website;

				MessageDialogInfo<Window, string, object, string, ustring[]> messageDialogInfo =
                    new
                    (
                        parent: aboutDialogInfo.Parent,
                        modal: aboutDialogInfo.Modal,
                        title: aboutDialogInfo.Title,
                        dialogFlags: null,
                        messageType: "Info",
                        buttonsType: ["Ok"],
                        message: messageTemp,
                        response: response
                    );

				if (ShowMessageDialog(ref messageDialogInfo, ref errorMessage))
                {
                    response = messageDialogInfo.Response;

                    if (!string.IsNullOrWhiteSpace(response))
                    {
                        aboutDialogInfo.Response = response;
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            aboutDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a printer.
        /// Static.
        /// </summary>
        /// <param name="printerDialogInfo">ref PrinterDialogInfo<Window, string, string></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns></returns>
        public static bool GetPrinter
        (
            ref PrinterDialogInfo<Window, string, string> printerDialogInfo,
            ref string errorMessage
        )
        {
			const bool returnValue = default;
            //string printerResponse = null;

            try
            {//TODO:convert to use ShowMessageDialog
                throw new NotImplementedException("GetPrinter");
                // Console.Write(printerDialogInfo.Title,": ");
                // //TODO:list available printers
                // printerResponse = Console.ReadLine();
                // if (!string.IsNullOrWhiteSpace(printerResponse))
                // {
                //     printerDialogInfo.Name = printerResponse;
                //     printerDialogInfo.Response = "OK";
                //     returnValue = true;
                // }
                // else
                // {
                //     printerDialogInfo.Response = "Cancel";
                //     returnValue = true;
                // }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            printerDialogInfo.BoolResult = returnValue;
            return returnValue;
        }
        /// <summary>
        /// Shows a message dialog.
        /// Static.
        /// Ex:MessageBox.Query(10, 7, "About", "Terminal.Gui Console1 by Stephen J Sepan\nUsing Terminal.Gui (gui.cs) by Miguel DeIcaza\nBased on demo by Ali Bahraminezhad\nVersion 0.1", "Ok");
        /// </summary>
        /// <param name="messageDialogInfo">ref MessageDialogInfo<Window, string, object, string,  ustring[]></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool ShowMessageDialog
        (
            ref MessageDialogInfo<Window, string, object, string,  ustring[]> messageDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
			try
			{
				int response = MessageBox.Query
                (
                    title: messageDialogInfo.Title,
                    message: messageDialogInfo.Message,
                    buttons: messageDialogInfo.ButtonsType //new NStack.ustring[] {"Ok", "Cancel"}
                );

                messageDialogInfo.Response = messageDialogInfo.ButtonsType[response].ToString();
                returnValue = true;
				//Note:code is sensitive to order of buttons
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }

            messageDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a color.
        /// Static.
        /// </summary>
        /// <param name="colorDialogInfo">ref ColorDialogInfo<Window, string, string></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetColor
        (
            ref ColorDialogInfo<Window, string, string> colorDialogInfo,
            ref string errorMessage
        )
        {
            bool returnValue = default;
            //string response = null;

            try
            {//TODO:convert to use ShowMessageDialog
                throw new NotImplementedException("GetColor");
                // Console.Write(colorDialogInfo.Title,": ");
                // response = Console.ReadLine();
                // if (!string.IsNullOrWhiteSpace(response))
                // {
                //     colorDialogInfo.Color = response;
                //     colorDialogInfo.Response = response;
                //     returnValue = true;
                // }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }

            colorDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        /// <summary>
        /// Get a font descriptor.
        /// Static.
        /// </summary>
        /// <param name="fontDialogInfo">ref FontDialogInfo<Window, string, string></param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        public static bool GetFont
        (
            ref FontDialogInfo<Window, string, string> fontDialogInfo,//Pango.FontDescription fontDescription,
            ref string errorMessage
        )
        {
            bool returnValue = default;
            //string response = null;

            try
            {//TODO:convert to use ShowMessageDialog
                throw new NotImplementedException("GetFont");
                // Console.Write(fontDialogInfo.Title,": ");
                // response = Console.ReadLine();
                // if (!string.IsNullOrWhiteSpace(response))
                // {
                //     fontDialogInfo.FontDescription = response;
                //     fontDialogInfo.Response = response;
                //     returnValue = true;
                // }

            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }

            fontDialogInfo.BoolResult = returnValue;
            return returnValue;
        }

        // /// <summary>
        // /// Perform input of connection string and provider name.
        // /// Uses MS Data Connections Dialog.
        // /// Note: relies on MS-LPL license and code from http://archive.msdn.microsoft.com/Connection
        // /// </summary>
        // /// <param name="connectionString"></param>
        // /// <param name="providerName"></param>
        // /// <param name="errorMessage"></param>
        // /// <returns></returns>
        // public static bool GetDataConnection
        // (
        //     ref string connectionString,
        //     ref string providerName,
        //     ref string errorMessage
        // )
        // {
        //     bool returnValue = default(bool);
        //     //DataConnectionDialog dataConnectionDialog = default(DataConnectionDialog);
        //     //DataConnectionConfiguration dataConnectionConfiguration = default(DataConnectionConfiguration);

        //     try
        //     {
        //         //dataConnectionDialog = new DataConnectionDialog();

        //         //DataSource.AddStandardDataSources(dataConnectionDialog);

        //         //dataConnectionDialog.SelectedDataSource = DataSource.SqlDataSource;
        //         //dataConnectionDialog.SelectedDataProvider = DataProvider.SqlDataProvider;//TODO:use?

        //         //dataConnectionConfiguration = new DataConnectionConfiguration(null);
        //         //dataConnectionConfiguration.LoadConfiguration(dataConnectionDialog);

        //         //(don't) set to current connection string, because it overwrites previous settings, requiring user to click Refresh in Data Connection Dialog.
        //         //dataConnectionDialog.ConnectionString = connectionString;

        //         if (true/*DataConnectionDialog.Show(dataConnectionDialog) == DialogResult.OK*/)
        //         {
        //             ////extract connection string
        //             //connectionString = dataConnectionDialog.ConnectionString;
        //             //providerName = dataConnectionDialog.SelectedDataProvider.ViewName;

        //             ////writes provider selection to xml file
        //             //dataConnectionConfiguration.SaveConfiguration(dataConnectionDialog);

        //             ////save these too
        //             //dataConnectionConfiguration.SaveSelectedProvider(dataConnectionDialog.SelectedDataProvider.ToString());
        //             //dataConnectionConfiguration.SaveSelectedSource(dataConnectionDialog.SelectedDataSource.ToString());

        //             returnValue = true;
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         errorMessage = ex.Message;
        //         Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

        //     }
        //     return returnValue;
        // }
        #endregion Methods
    }
}
