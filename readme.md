# readme.md - README for Ssepan.Application.Core.GuiCs

## TODO

### Purpose

To encapsulate common application functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

## Instructions/example of adding Terminal.GUI to dotnet

dotnet add package Terminal.Gui --version 1.17.0

### History

5.6:
~upgrade Terminal.Gui from 1.16.0 to 1.17.0 via NuGet

5.5:
~upgrade Terminal.Gui from 1.15.1 to 1.16.0 via NuGet

5.4:

~refactor to newer C# features
~refactor to existing language types
~perform format linting
~redo command-line args and how passed filename overrides config file settings, including addition of a format arg.

5.3:
~Update to .net8.0

5.2:
~move usings
~implement dlg filters
~trap new filenames
~ use dlg info BoolResult
~use ACTION_ consts in status
~set dlg titles to action not appname
~move progress/status from view to VM

5.1:
~use new Website property in AssemblyInfoBase, and set in AssemblyInfo for modules that will display About.

5.0:
~Refactor Ssepan.Application.Core.GtkSharp into several libraries. Ssepan.Application.Core.GuiCs will hold classes for an upcoming Text UI (TUI) project using Terminal.Gui (AKA Gui.cs) package.

Steve Sepan
<sjsepan@yahoo.com>
3/5/2024
